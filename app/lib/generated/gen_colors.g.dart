// DO NOT EDIT. This is code generated via package:easy_colors/generate.dart

import 'package:flutter/painting.dart';

class EasyColors {
  const EasyColors._();
  
  static const Color loginBackground = Color(4281217636);
  static const Color loginSplashScreenLoader = Color(4278228735);
  static const Color loginSplashScreenText = Color(4294967295);
  static const Color loginButtonBackground = Color(4294967295);
  static const Color loginButtonText = Color(4278190080);
  static const Color loginDisclaimerBackground = Color(4294428160);
  static const Color loginDisclaimerText = Color(4278190080);
  static const Color loginDataInputBackground = Color(0);
  static const Color loginDataInputText = Color(4292467161);
  static const Color loginDataInputBorder = Color(4294967295);
  static const Color loginDataInputIcon = Color(4294967295);
  static const Color loginDataInputError = Color(4294901760);
  static const Color loginDataSwitchActiveTrack = Color(4290435669);
  static const Color loginDataSwitchActiveThumb = Color(4294967295);
  static const Color loginDataSwitchTrack = Color(4286611584);
  static const Color loginDataSwitchThumb = Color(4294967295);
  static const Color navBarBackground = Color(4281217636);
  static const Color navBarItemText = Color(4294967295);
  static const Color navBarItemIcon = Color(4294967295);
  static const Color navBarDivider = Color(4288585374);
  static const Color accountHeader = Color(4280838803);
  static const Color accountUserCircle = Color(4288585374);
  static const Color accountUserText = Color(4294967295);
  static const Color background = Color(4294967295);
  static const Color tableBorder = Color(4278190080);
  static const Color tableText = Color(4278190080);
  static const Color newsEntryBackground = Color(4294967295);
  static const Color newsEntryBackgroundShadow = Color(4288585374);
  static const Color newsEntryDateColor = Color(4278190080);
  static const Color settingsEntryBackground = Color(4294967295);
  static const Color settingsEntryBackgroundShadow = Color(4288585374);
  static const Color settingsEntryColor = Color(4278190080);
  static const Color settingsEntryActiveColor = Color(4280043878);
  static const Color homeTitleColor = Color(4278190080);
}
