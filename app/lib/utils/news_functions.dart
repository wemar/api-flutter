import 'dart:convert';

import 'package:app/views/home/components/news_entry.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;

import 'api_functions.dart';
import 'functions.dart';

class NewsFunctions {
  void getNews(context, successCallback, failureCallback) async {
    // Calls API an returns List of NewsEntryWidgets
    ApiFunctions().callAPI(
      "/news",
      "{}",
      "get",
      context,
      (http.Response response) {
        try {
          List<Widget> newsEntries = [];
          for (var entry in (jsonDecode(response.body) as List)) {
            ImageProvider imageProvider;
            if (entry["news_image"].isEmpty) {
              imageProvider =
                  const AssetImage("assets/images/itech/logo_icons_only.png");
            } else {
              imageProvider = NetworkImage(entry["news_image"]);
            }
            var news = NewsEntryWidget(
              body: entry["news_body"],
              imageProvider: imageProvider,
              date: formatDate(entry["news_date_from"]),
            );
            newsEntries.add(
              Column(
                children: [
                  news,
                  const SizedBox(height: 20),
                ],
              ),
            );
          }
          // return newsEntrys
          successCallback(newsEntries);
        } on Error {
          failureCallback();
        }
      },
      (statusCode) {
        failureCallback();
        if (statusCode != "api_offline")
          {
            Functions.showInfoDialog(
              context,
              translate("errors.internalError.dialog"),
              [
                Text(
                  translate("errors.internalError.description"),
                ),
              ],
              translate("errors.internalError.action"),
            );
          }
      },
    );
  }

  String formatDate(String date) {
    // formats date to translated version
    //OUTPUT DE Dienstag, der 14.01.2003
    //OUTPUT EN Tuesday, the 14.01.2003

    String dateFormat = translate("general.date_format");
    try {
      DateTime dateTime = DateTime.parse(date);
      String day = dateTime.day.toString();
      String month = dateTime.month.toString();
      String year = dateTime.year.toString();

      dateFormat = dateFormat.replaceAll("%day", day);
      dateFormat = dateFormat.replaceAll("%month", month);
      dateFormat = dateFormat.replaceAll("%year", year);
      return dateFormat;
    } on FormatException catch (_, e) {
      return date;
    }
  }
}
