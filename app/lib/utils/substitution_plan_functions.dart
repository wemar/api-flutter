import 'dart:convert';

import 'package:app/utils/api_functions.dart';
import 'package:app/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;

import '../models/substitution_plan.dart';

class SubstitutionPlanFunctions {
  void getSubstitutionsPlan(context, successCallback, failureCallback) async {
    // gets SubstitutionsPlan by requesting it from server
    ApiFunctions().callAPI(
      "/standin",
      "{}",
      "get",
      context,
      (http.Response response) {
        try {
          SubstitutionPlan substitutionPlan =
              SubstitutionPlan.fromJson(jsonDecode(response.body));
          successCallback(substitutionPlan);
        } on Error {
          failureCallback();
        }
        // return SubstitutionPlan
      },
      (statusCode) {
        failureCallback();
        if (statusCode != "api_offline")
          {
            Functions.showInfoDialog(
              context,
              translate("errors.internalError.dialog"),
              [
                Text(
                  translate("errors.internalError.description"),
                ),
              ],
              translate("errors.internalError.action"),
            );
          }
      },
    );
  }

  String formatDate(int date, String weekDay) {
    // formats date from UNTIS Format to translated version
    //Input 20030114
    //OUTPUT DE Dienstag, der 14.01.2003
    //OUTPUT EN Tuesday, the 14.01.2003

    String titleFormat = translate("substitutions_table.title_format");
    weekDay = translate("substitutions_table.days.$weekDay");

    String dateData = date.toString();
    String day = dateData.substring(6, 8);
    String month = dateData.substring(4, 6);
    String year = dateData.substring(0, 4);

    //replace placeholders from lang.json
    titleFormat = titleFormat.replaceAll("%week_day", weekDay);
    titleFormat = titleFormat.replaceAll("%day", day);
    titleFormat = titleFormat.replaceAll("%month", month);
    titleFormat = titleFormat.replaceAll("%year", year);
    return titleFormat;
  }
}
