import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:shared_preferences/shared_preferences.dart';

// All code that is used repeatedly
class Functions {
  // creates and DialogWidget with children
  static showInfoDialog(
      context, String title, List<Widget> widgets, String textButton) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: widgets,
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(textButton),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static Function onLoading(context) {
    // creates loading animation and a callback to hide it
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Container(
            margin: const EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const CircularProgressIndicator(),
                const SizedBox(height: 50),
                Text(translate("general.loading")),
              ],
            ),
          ),
        );
      },
    );
    callback() {
      Navigator.pop(context); //pop dialog (removes the dialog)
    }

    return callback;
  }

  Future<void> showLanguageMenu(context) async {
    // shows bottom popup for changing language
    LocalizationDelegate localizationDelegate =
        LocalizedApp.of(context).delegate;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Wrap(
          children: [
            const SizedBox(height: 40),
            ListTile(
              leading: const Text(""),
              title: Text(translate("general.languages.de")),
              onTap: () {
                // hides menu and sets locale
                Navigator.pop(context);
                sharedPreferences.setString("locale", "de");
                localizationDelegate.changeLocale(const Locale("de"));
              },
            ),
            const SizedBox(height: 40),
            ListTile(
              leading: const Text(""),
              title: Text(translate("general.languages.en")),
              onTap: () {
                // hides menu and sets locale
                Navigator.pop(context);
                sharedPreferences.setString("locale", "en");
                localizationDelegate.changeLocale(const Locale("en"));
              },
            ),
            const SizedBox(height: 40),
          ],
        );
      },
    );
  }

  Route createRoute(target) {
    // creates a route with no animation
    return PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => target,
        transitionDuration: Duration.zero);
  }
}
