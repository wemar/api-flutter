extension StringExtension on String {
  String capitalize() {
    // capitalizes first letter of String
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}