import 'package:flutter/material.dart';


class TableFunctions {
  // creates table parts
  static Column headerEntry(String text, double heightFactor) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Center(
          heightFactor: heightFactor,
          child: Text(
            text,
            style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  static TableRow listEntry(List entries, double heightFactor) {
    return TableRow(
      children: entries.map((entry) {
        return TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Center(
            heightFactor: heightFactor,
            child: Text(
              entry,
              textAlign: TextAlign.center,
            ),
          ),
        );
      }).toList(),
    );
  }
}
