import 'dart:convert';

import 'package:app/models/holiday_plan.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;

import 'api_functions.dart';
import 'functions.dart';

class HolidayPlanFunctions {
  void getHolidayPlan(context, successCallback, failureCallback) async {
    ApiFunctions().callAPI(
      "/holiday",
      "{}",
      "get",
      context,
      (http.Response response) {
        try {
          HolidayPlan holidayPlan =
              HolidayPlan.fromJson(jsonDecode(response.body));
          // return HolidayPlan
          successCallback(holidayPlan);
        } on Error {
          failureCallback();
        }
      },
      (statusCode) {
        failureCallback();
        if (statusCode != "api_offline")
          {
            Functions.showInfoDialog(
              context,
              translate("errors.internalError.dialog"),
              [
                Text(
                  translate("errors.internalError.description"),
                ),
              ],
              translate("errors.internalError.action"),
            );
          }
      },
    );
  }

  String formatDate(DateTime dateTime) {
    // formats date to translated version
    //Input DateTime Object
    //OUTPUT DE Dienstag, der 14.01.2003
    //OUTPUT EN Tuesday, the 14.01.2003

    String titleFormat = translate("general.date_format");

    String day = dateTime.day.toString();
    String month = dateTime.month.toString();
    String year = dateTime.year.toString();

    titleFormat = titleFormat.replaceAll("%day", day);
    titleFormat = titleFormat.replaceAll("%month", month);
    titleFormat = titleFormat.replaceAll("%year", year);
    return titleFormat;
  }
}
