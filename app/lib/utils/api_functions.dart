import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'functions.dart';

class ApiFunctions {
  void callAPI(String path, body, method, context, successCallback,
      failureCallback) async {
    // create uri for api (http://api.itech-bs14.de)
    final uri = Uri.parse('https://api.itech-bs14.de$path');
    // sets headers
    final headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
    if (!["/login", "/register", "/holiday"].contains(path)){
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      final token = sharedPreferences.getString("authToken");
      headers["Authorization"] ="Bearer $token";
    }
    // checks if API is reachable
    checkApiReachability(context, uri, (reachable) async {
      if (reachable) {
        // api is reachable
        if (method == "post") {
          // makes http request to API
          final response = await http.post(uri, headers: headers, body: body);
          if (response.statusCode == 200) {
            // return value if successful
            successCallback(response);
          } else {
            // return statusCode in case of error
            failureCallback(response.statusCode);
          }
        }
        if (method == "get"){
          final response = await http.get(uri, headers: headers);
          if (response.statusCode == 200) {
            // return value if successful
            successCallback(response);
          } else {
            // return statusCode in case of error
            failureCallback(response.statusCode);
          }
        }
      } else {
        // return error that api is offline
        failureCallback("api_offline");
        // shows info Dialog
        Functions.showInfoDialog(
          context,
          translate("errors.reachability.dialog"),
          [
            Text(
              translate("errors.reachability.description"),
            ),
          ],
          translate("errors.reachability.action"),
        );
      }
    });
  }

  void checkApiReachability(context, uri, Function reachabilityStatus) {
    Future<http.Response> response = http.get(uri);

    // cancels request if timer is exceeded
    Timer timer = Timer(const Duration(seconds: 5), () {
      response.ignore();
      reachabilityStatus(false);
    });

    // creates responses
    response.then((response) {
      if (response.statusCode != 500) {
        reachabilityStatus(true);
      } else {
        reachabilityStatus(false);
      }
      return response;
    });
    response.catchError((error) {
      reachabilityStatus(false);
      return response;
    });
    timer.cancel();
  }
}
