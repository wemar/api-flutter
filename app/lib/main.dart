import 'dart:io';

import 'package:app/views/splash_screen/splash_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  try{
    ByteData data =
    await PlatformAssetBundle().load('assets/ca/itech-bs14-de.pem');
    SecurityContext.defaultContext
        .setTrustedCertificatesBytes(data.buffer.asUint8List());
  }
  on Error{
    if (kDebugMode) {
      print("Browser Platform");
    }
  }


  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  // set up app with translations
  if (kDebugMode) {
    print(sharedPreferences.getString("locale") ?? "de");
  }
  var delegate = await LocalizationDelegate.create(
    fallbackLocale: sharedPreferences.getString("locale") ?? "de",
    supportedLocales: ["en", "de"],
  );
  delegate.changeLocale(
    Locale(sharedPreferences.getString("locale") ?? "de"),
  );
  // starts app
  runApp(LocalizedApp(delegate, const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var localizationDelegate = LocalizedApp.of(context).delegate;

    // localisation config
    return LocalizationProvider(
      state: LocalizationProvider.of(context).state,
      child: MaterialApp(
        title: "ITECH BS14",
        localizationsDelegates: GlobalMaterialLocalizations.delegates,
        supportedLocales: localizationDelegate.supportedLocales,
        locale: localizationDelegate.currentLocale,
        theme: ThemeData(fontFamily: 'Roboto-Regular'),
        home: const SplashScreen(),
      ),
    );
  }
}
