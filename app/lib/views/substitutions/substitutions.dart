import 'package:app/generated/gen_colors.g.dart';
import 'package:app/models/substitution_plan.dart';
import 'package:app/views/substitutions/components/substitution_table.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../utils/functions.dart';
import '../../utils/substitution_plan_functions.dart';
import '../components/base_structure_listview.dart';
import '../components/reachability_info.dart';
import '../navigation/NavDrawer.dart';

class SubstitutionScreen extends StatefulWidget {
  const SubstitutionScreen({Key? key}) : super(key: key);

  @override
  LoginTemplateState createState() => LoginTemplateState();
}

class LoginTemplateState extends State<SubstitutionScreen> {
  // shows SubstitutionPlan

  static const String routeName = '/substitution';
  SubstitutionPlan substitutionPlan = SubstitutionPlan.fromJson({"dates": []});
  Function loading = () {};
  int viewState = 0;

  @override
  void initState() {
    // functions is called only once
    getData(context); // async
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // functions is called on every render attempt
    Widget body = const Text("loading");
    switch (viewState) {
      // changes view according to state
      case (0):
        {
          body = BaseStructureListview().show(context, [
            SubstitutionTable(substitutionPlan: substitutionPlan, lehrer: true)
          ]);
          break;
        }
      case (1):
        {
          // shows "Service is unreachable Status"
          body = ReachabilityInfo().show();
          break;
        }
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: EasyColors.navBarBackground,
        title: Text(translate("substitutions_table.title")),
      ),
      backgroundColor: EasyColors.background,
      drawer: NavDrawer(),
      body: RefreshIndicator(
        onRefresh: () async {
          getData(context);
        },
        child: ListView(children: [body]),
      ),
    );
  }

  void getData(context) {
    // gets data for Substitutes
    WidgetsBinding.instance
        .addPostFrameCallback((_) => loading = Functions.onLoading(context));
    SubstitutionPlanFunctions().getSubstitutionsPlan(
      context,
      (plan) {
        setState(
          () {
            // changes the SubstitutionsPlan
            substitutionPlan = plan;
          },
        );
        loading();
        setState(
          () {
            // shows plan
            viewState = 0;
          },
        );
      },
      () {
        loading();
        setState(() {
          // shows "Service is unreachable Status"
          viewState = 1;
        });
      },
    );
  }
}
