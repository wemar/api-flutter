import 'package:app/generated/gen_colors.g.dart';
import 'package:app/models/substitution_plan.dart';
import 'package:app/utils/substitution_plan_functions.dart';
import 'package:app/utils/table_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

// All Widgets to create a SubstitutionTable

class SubstitutionTable extends StatefulWidget {
  const SubstitutionTable(
      {Key? key, required this.substitutionPlan, required this.lehrer})
      : super(key: key);
  final SubstitutionPlan substitutionPlan;
  final bool lehrer;

  @override
  SubstitutionTableState createState() => SubstitutionTableState();
}

class SubstitutionTableState extends State<SubstitutionTable> {
  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];
    for (var day in widget.substitutionPlan.dates) {
      children.add(const SizedBox(height: 20));
      children.add(
          day.results.isNotEmpty ? _table(day, widget.lehrer) : _emptyDay(day));
    }

    return Column(
      children: children,
    );
  }

  Column _table(SubstitutionDate substitutionDate, bool teacher) {
    List<TableRow> children = [_header(teacher)];

    for (var substitutionResult in substitutionDate.results) {
      List entries = [substitutionResult.hour, substitutionResult.info];
      if (teacher) {
        entries.insert(0, substitutionResult.classname);
      }
      children.add(TableFunctions.listEntry(entries, 1));
    }

    return Column(
      children: [
        Text(
          SubstitutionPlanFunctions().formatDate(
            substitutionDate.date,
            substitutionDate.weekDay,
          ),
        ),
        const SizedBox(height: 10),
        Table(
          border: TableBorder.all(
            color: EasyColors.tableBorder,
            style: BorderStyle.solid,
            width: 0.7,
          ),
          children: children,
        ),
      ],
    );
  }

  TableRow _header(bool teacher) {
    List<String> entries = [
      translate("substitutions_table.table.hour"),
      translate("substitutions_table.table.info"),
    ];
    if (teacher) {
      entries.insert(0, translate("substitutions_table.table.class"));
    }

    return TableRow(
      children: entries.map((entry) {
        return TableFunctions.headerEntry(entry, 2);
      }).toList(),
    );
  }

  Container _emptyDay(SubstitutionDate substitutionDate) {
    return Container(
      padding: const EdgeInsets.only(top: 20),
      child: Column(
        children: [
          Text(
            SubstitutionPlanFunctions().formatDate(
              substitutionDate.date,
              substitutionDate.weekDay,
            ),
          ),
          Text(translate("substitutions_table.no_substitutions"))
        ],
      ),
    );
  }
}
