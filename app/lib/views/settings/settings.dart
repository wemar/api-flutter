import 'package:app/generated/gen_colors.g.dart';
import 'package:app/views/navigation/NavDrawer.dart';
import 'package:app/views/settings/components/settings_entry.dart';
import 'package:app/views/settings/components/settings_profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  SettingsScreenState createState() => SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  static const String routeName = '/settings';

  @override
  Widget build(BuildContext context) {
    // creates structure for settings menu
    var localizationDelegate = LocalizedApp.of(context).delegate;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: EasyColors.navBarBackground,
        title: Text(translate("settings.title")),
      ),
      drawer: NavDrawer(),
      backgroundColor: EasyColors.background,
      body: ListView(
        children: [
          const SettingsProfile(
            title: "Username",
            subtitle: "Classname",
            email: "email",
          ),
          SettingEntry(
            title: translate("general.languages.title"),
            widgets: localizationDelegate.supportedLocales.map((locale) {
              return ListTile(
                title: Text(
                  translate("general.languages.$locale"),
                ),
                onTap: () {
                  changeLocale(context, locale.toString());
                },
              );
            }).toList(),
            icon: Icons.language,
          ),
        ],
      ),
    );
  }
}
