import 'package:app/generated/gen_colors.g.dart';
import 'package:flutter/material.dart';

class SettingsProfile extends StatelessWidget {
  const SettingsProfile(
      {Key? key,
      required this.title,
      required this.subtitle,
      required this.email})
      : super(key: key);
  final String title;
  final String subtitle;
  final String email;

  @override
  Widget build(BuildContext context) {
    // Creates SettingsHeader
    return Container(
      color: EasyColors.accountHeader,
      child: Padding(
        padding:
            const EdgeInsets.only(top: 30, bottom: 30, right: 10, left: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 100,
              width: 100,
              child: CircleAvatar(
                backgroundColor: EasyColors.accountUserCircle,
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Image(
                    image: AssetImage(
                        "assets/images/fontawesome/user-astronaut-solid.png"),
                  ),
                ),
              ),
            ),
            const SizedBox(width: 10),
            Flexible(
              child: SizedBox(
                height: 75,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: const TextStyle(color: EasyColors.accountUserText),
                    ),
                    Text(
                      subtitle,
                      style: const TextStyle(color: EasyColors.accountUserText),
                    ),
                    Text(
                      email,
                      style: const TextStyle(color: EasyColors.accountUserText),
                      overflow: TextOverflow.clip,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
