import 'package:app/generated/gen_colors.g.dart';
import 'package:flutter/material.dart';

class SettingEntry extends StatefulWidget {
  const SettingEntry(
      {Key? key,
      required this.title,
      required this.widgets,
      required this.icon})
      : super(key: key);
  final String title;
  final List<Widget> widgets;
  final IconData icon;

  @override
  SettingEntryState createState() => SettingEntryState();
}

class SettingEntryState extends State<SettingEntry> {
  @override
  Widget build(BuildContext context) {
    // represents individual SettingsTile
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10, top: 20),
      decoration: BoxDecoration(
        color: EasyColors.settingsEntryBackground,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: EasyColors.settingsEntryBackgroundShadow.withOpacity(0.2),
            spreadRadius: 3,
            blurRadius: 7,
          ),
        ],
      ),
      child: Padding(
        padding:
            const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
        child: ExpansionTile(
            collapsedIconColor: EasyColors.settingsEntryColor,
            collapsedTextColor: EasyColors.settingsEntryColor,
            textColor: EasyColors.settingsEntryActiveColor,
            iconColor: EasyColors.settingsEntryActiveColor,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(widget.icon),
                Text(widget.title),
              ],
            ),
            children: widget.widgets),
      ),
    );
  }
}
