import 'package:app/generated/gen_colors.g.dart';
import 'package:app/models/holiday_plan.dart';
import 'package:app/utils/holiday_plan_functions.dart';
import 'package:app/utils/string_extension.dart';
import 'package:app/utils/table_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class HolidayTable extends StatefulWidget {
  const HolidayTable({Key? key, required this.holidayPlan}) : super(key: key);
  final HolidayPlan holidayPlan;

  @override
  HolidayTableState createState() => HolidayTableState();
}

class HolidayTableState extends State<HolidayTable> {
  @override
  Widget build(BuildContext context) {
    // creates holidayTable by supplied data
    List<TableRow> children = [];
    if (widget.holidayPlan.entries.isNotEmpty){
      children.add(_header());
    }
    for (var holidayPlanEntry in widget.holidayPlan.entries) {
      List<String> entries = [
        holidayPlanEntry.name.capitalize(),
        HolidayPlanFunctions().formatDate(holidayPlanEntry.start),
        HolidayPlanFunctions().formatDate(holidayPlanEntry.end),
      ];
      children.add(TableFunctions.listEntry(entries, 2));
    }
    return Column(
      children: [
        const SizedBox(height: 10),
        Table(
          border: TableBorder.all(
            color: EasyColors.tableBorder,
            style: BorderStyle.solid,
            width: 0.7,
          ),
          children: children,
        ),
      ],
    );
  }

  TableRow _header() {
    List<String> entries = [
      translate("holiday_table.table.name"),
      translate("holiday_table.table.start"),
      translate("holiday_table.table.end"),
    ];

    return TableRow(
      children: entries.map((entry) {
        return TableFunctions.headerEntry(entry, 3);
      }).toList(),
    );
  }
}
