import 'package:app/generated/gen_colors.g.dart';
import 'package:app/models/holiday_plan.dart';
import 'package:app/views/holiday/components/holiday_table.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../../utils/functions.dart';
import '../../utils/holiday_plan_functions.dart';
import '../components/reachability_info.dart';
import '../components/base_structure_listview.dart';
import '../navigation/NavDrawer.dart';

class HolidayScreen extends StatefulWidget {
  const HolidayScreen({Key? key}) : super(key: key);

  @override
  HolidayScreenState createState() => HolidayScreenState();
}

class HolidayScreenState extends State<HolidayScreen> {
  // shows HolidayPlan

  static const String routeName = '/holidays';
  HolidayPlan holidayPlan = HolidayPlan.fromJson([]);
  Function loading = () {};
  int viewState = 0;

  @override
  void initState() {
    // functions is called only once
    getData(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // functions is called on every render attempt
    Widget body = const Text("loading");
    switch (viewState) {
      case (0):
        {
          body = BaseStructureListview().show(context, [
            HolidayTable(
              holidayPlan: holidayPlan,
            ),
          ]);
          break;
        }
      case (1):
        {
          body = body = ReachabilityInfo().show();
          break;
        }
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: EasyColors.navBarBackground,
        title: Text(translate("holiday_table.title")),
      ),
      backgroundColor: EasyColors.background,
      drawer: NavDrawer(),
      body: RefreshIndicator(
        onRefresh: () async {
          getData(context);
        },
        child: ListView(children: [body]),
      ),
    );
  }

  void getData(context) {
    WidgetsBinding.instance
        .addPostFrameCallback((_) => loading = Functions.onLoading(context));
    HolidayPlanFunctions().getHolidayPlan(
      context,
      (plan) {
        setState(
          () {
            // changes the SubstitutionsPlan
            holidayPlan = plan;
          },
        );
        loading();
        setState(
          () {
            viewState = 0;
          },
        );
      },
      () {
        loading();
        setState(() {
          viewState = 1;
        });
      },
    );
  }
}
