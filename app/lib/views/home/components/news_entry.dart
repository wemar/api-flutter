import 'package:app/generated/gen_colors.g.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsEntryWidget extends StatelessWidget {
  const NewsEntryWidget(
      {Key? key,
      required this.body,
      required this.imageProvider,
      required this.date})
      : super(key: key);
  final String body;
  final String date;
  final ImageProvider imageProvider;

  @override
  Widget build(BuildContext context) {
    // creates NewsEntry with url_open handler

    // hides null possible annotation
    Widget widget = Container();

    Widget imageWidget = Image(
      image: imageProvider,
      width: MediaQuery.of(context).size.width * 0.9,
    );

    Future<void> launchOpen(url) async {
      await launchUrl(url, mode: LaunchMode.externalNonBrowserApplication);
    }

    Widget htmlWidget = Html(
      data: body,
      onLinkTap: (url, _, __, ___) {
        if (url != null) {
          launchOpen(Uri.parse(url));
        }
      },
    );

    if (MediaQuery.of(context).size.width < 700) {
      widget = Column(
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: 20.0,
              maxHeight: MediaQuery.of(context).size.height / 3,
            ),
            child: imageWidget,
          ),
          const SizedBox(height: 10),
          Text(
            date,
            style: const TextStyle(color: EasyColors.newsEntryDateColor),
          ),
          const SizedBox(height: 10),
          htmlWidget
        ],
      );
    } else {
      widget = Row(
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: 20.0,
              maxWidth: MediaQuery.of(context).size.width / 6,
            ),
            child: imageWidget,
          ),
          const SizedBox(width: 10),
          Column(
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * 0.70,
                ),
                child: htmlWidget,
              ),
              const SizedBox(height: 10),
              Text(date),
            ],
          ),
        ],
      );
    }

    return Container(
      margin: const EdgeInsets.only(right: 20, left: 20),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        color: EasyColors.newsEntryBackground,
        boxShadow: [
          BoxShadow(
            color: EasyColors.newsEntryBackgroundShadow.withOpacity(0.2),
            spreadRadius: 3,
            blurRadius: 7,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            widget,
          ],
        ),
      ),
    );
  }
}
