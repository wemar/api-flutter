import 'package:app/generated/gen_colors.g.dart';
import 'package:app/utils/news_functions.dart';
import 'package:app/views/components/reachability_info.dart';
import 'package:app/views/components/base_structure_listview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../utils/functions.dart';
import '../navigation/NavDrawer.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  static const String routeName = '/home';
  List<Widget> newsArticles = [];
  Function loading = () {};
  int viewState = 0;

  @override
  void initState() {
    getData(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //building component structure
    Widget body = const Center(child: Text("loading"));
    switch (viewState) {
      case (0):
        {
          body = BaseStructureListview().show(context, newsArticles);
          break;
        }
      case (1):
        {
          body = ReachabilityInfo().show();
          break;
        }
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: EasyColors.navBarBackground,
        title: Text(translate("home.title")),
      ),
      backgroundColor: EasyColors.background,
      drawer: NavDrawer(),
      body: RefreshIndicator(
        onRefresh: () async {
          getData(context);
        },
        child: ListView(children: [body]),
      ),
    );
  }

  void getData(context) {
    // gets data for newsEntries
    WidgetsBinding.instance
        .addPostFrameCallback((_) => loading = Functions.onLoading(context));
    NewsFunctions().getNews(
      context,
      (news)  {
        setState(
          () {
            // changes the newsArticles
            newsArticles = news;
          },
        );
        loading();
        setState(
          () {
            viewState = 0;
          },
        );
      },
      () {
        loading();
        setState(() {
          viewState = 1;
        });
      },
    );
  }
}
