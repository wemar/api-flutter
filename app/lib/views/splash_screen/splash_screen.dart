import 'package:app/generated/gen_colors.g.dart';
import 'package:app/utils/functions.dart';
import 'package:flutter/material.dart';

import '../../controllers/auth.dart';
import '../home/home.dart';
import '../login/welcome.dart';

//Shows the SplashScreen an checks to loginState
class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    loginControl();
  }

  void loginControl() {
    AuthController().isLoginUser(
      context,
      () {
        Navigator.of(context).pushReplacement(
          Functions().createRoute(
            const HomeScreen(),
          ),
        );
      },
      () {
        Navigator.of(context).pushReplacement(
          Functions().createRoute(
            const LoginWelcomeScreen(),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // creates SplashScreen on App start. Used to check if user is still logged in
    return Scaffold(
      appBar: null,
      backgroundColor: EasyColors.loginBackground,
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              Image(
                image: AssetImage('assets/images/itech/logo_icons_only.png'),
              ),
              Text(
                "ITECH BS14",
                style: TextStyle(
                  color: EasyColors.loginSplashScreenText,
                  fontSize: 30,
                  letterSpacing: 2,
                ),
              ),
              SizedBox(
                height: 40,
              ),
              //loading circle
              CircularProgressIndicator(
                color: EasyColors.loginSplashScreenLoader,
              )
            ],
          ),
        ),
      ),
    );
  }
}
