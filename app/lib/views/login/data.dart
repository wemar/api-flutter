import 'package:app/controllers/auth.dart';
import 'package:app/generated/gen_colors.g.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../../utils/functions.dart';
import 'components/buttons.dart';
import 'components/input_field.dart';
import 'components/login_template.dart';

class LoginDataScreen extends StatefulWidget {
  const LoginDataScreen({Key? key}) : super(key: key);

  @override
  LoginDataScreenState createState() => LoginDataScreenState();
}

class LoginDataScreenState extends State<LoginDataScreen> {
  // TextEditingController for every field to get text
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController codeController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // private state to change ui from login to register and back
  bool _registerActive = false;

  @override
  Widget build(BuildContext context) {
    //building page structure
    return LoaderOverlay(
      child: LoginTemplate(
        widgets: [
          Form(
            key: _formKey,
            child: Column(
              children: [
                InputField(
                  controller: usernameController,
                  labelText: translate("login.pages.data.username"),
                  icon: Icons.person,
                  obscureText: false,
                ),
                const SizedBox(height: 20),
                InputField(
                  controller: passwordController,
                  labelText: translate("login.pages.data.password"),
                  icon: Icons.password,
                  obscureText: true,
                ),
                const SizedBox(height: 20),
                Visibility(
                  // hides if _registerActive is false (token is not needed for register)
                  visible: !_registerActive,
                  child: Column(children: [
                    InputField(
                      controller: codeController,
                      labelText: translate("login.pages.data.token"),
                      icon: Icons.pin,
                      obscureText: false,
                    ),
                  ]),
                ),
                Visibility(
                  // hides if _registerActive is true(email is not needed for login)
                  visible: _registerActive,
                  child: Column(
                    children: [
                      InputField(
                        controller: emailController,
                        labelText: translate("login.pages.data.email"),
                        icon: Icons.email,
                        obscureText: false,
                      ),
                    ],
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      translate("login.buttons.register_action"),
                      style: const TextStyle(
                        fontSize: 14,
                        color: EasyColors.loginDataInputText,
                      ),
                    ),
                    Switch(
                      value: _registerActive,
                      // changes state of _registerActive on button press
                      onChanged: (bool state){
                        setState(
                          () {
                            _registerActive = state;
                          },
                        );
                      },
                      activeTrackColor: EasyColors.loginDataSwitchActiveTrack,
                      activeColor: EasyColors.loginDataSwitchActiveThumb,
                      inactiveThumbColor: EasyColors.loginDataSwitchTrack,
                      inactiveTrackColor: EasyColors.loginDataSwitchThumb,
                    ),
                  ],
                )
              ],
            ),
          ),
          const Spacer(),
          InputButton(
            action: validateAndSave,
            // changes text if _registerActive (login message for login and register message for register)
            text: _registerActive
                ? translate("login.buttons.register")
                : translate("login.buttons.login"),
            icon: Icons.login,
          ),
        ],
      ),
    );
  }

  void validateAndSave(context) {
    // shows loading animation and uses callback as hide function
    Function loading = Functions.onLoading(context);
    final FormState? form = _formKey.currentState;
    if (form != null) {
      if (form.validate()) {
        // Form is valid
        processLoginData(context, loading);
      } else {
        // Form is invalid
        loading();
      }
    }
  }

  void processLoginData(context, loaderCallback) {
    // calls different functions according to _registerActive
    if (_registerActive) {
      AuthController().registerUser(
        usernameController.text,
        passwordController.text,
        emailController.text,
        context,
        loaderCallback,
        () {
          //user is now registered
          Functions.showInfoDialog(
            context,
            translate("login.messages.register.dialog"),
            [
              Text(
                translate("login.messages.register.description"),
              ),
            ],
            translate("login.messages.register.action"),
          );
        },
        () {
          // error while registration
          Functions.showInfoDialog(
            context,
            translate("login.errors.register.dialog"),
            [
              Text(
                translate("login.errors.register.description"),
              ),
            ],
            translate("login.errors.register.action"),
          );
        },
      );
    } else {
      AuthController().loginUser(
        usernameController.text,
        passwordController.text,
        codeController.text,
        context,
        loaderCallback,
        () {
          Functions.showInfoDialog(
            context,
            translate("login.errors.login.dialog"),
            [
              Text(
                translate("login.errors.login.description"),
              ),
            ],
            translate("login.errors.login.action"),
          );
        },
      );
    }
  }
}
