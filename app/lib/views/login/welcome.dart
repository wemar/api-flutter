import 'package:app/utils/functions.dart';
import 'components/buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../../controllers/auth.dart';
import 'disclaimer.dart';
import 'components/login_template.dart';

class LoginWelcomeScreen extends StatelessWidget {
  const LoginWelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthController().logoutUser();
    return LoginTemplate(
      widgets: [
        InputButton(
          icon: Icons.language,
          text: "🇩🇪 / 🇬🇧",
          action: (context) {
            Functions().showLanguageMenu(context);
          },
        ),
        InputButton(
          // change location
          action: (context) {
            Navigator.of(context).push(
              Functions().createRoute(
                LoginDisclaimerScreen(),
              ),
            );
          },
          text: translate("login.buttons.welcome"),
          icon: Icons.start,
        ),
      ],
    );
  }
}
