import 'package:app/generated/gen_colors.g.dart';
import 'package:flutter/material.dart';

import 'login_header.dart';

class LoginTemplate extends StatefulWidget {
  const LoginTemplate({Key? key, required this.widgets}) : super(key: key);
  final List<Widget> widgets;

  @override
  LoginTemplateState createState() => LoginTemplateState();
}

class LoginTemplateState extends State<LoginTemplate> {
  //LoginTemplate for all login pages
  @override
  Widget build(BuildContext context) {
    widget.widgets.insert(0, const LoginHeader());
    widget.widgets.insert(1, const Spacer());
    return Scaffold(
      appBar: null,
      backgroundColor: EasyColors.loginBackground,
      body: SafeArea(
        left: false,
        right: false,
        child: Column(
          children: widget.widgets,
        ),
      ),
    );
  }
}
