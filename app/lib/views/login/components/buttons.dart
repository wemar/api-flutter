import 'package:app/generated/gen_colors.g.dart';
import 'package:flutter/material.dart';

class InputButton extends StatelessWidget {
  // set required params
  const InputButton(
      {Key? key, required this.action, required this.text, required this.icon})
      : super(key: key);

  //Callback-Action at button press
  final Function action;

  //Text on Button
  final String text;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    //building component structure
    return Container(
      width: 200,
      margin: const EdgeInsets.only(bottom: 20),
      height: 48,
      child: ElevatedButton.icon(
        icon: Icon(
          icon,
          color: EasyColors.loginButtonText,
        ),
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(
              EasyColors.loginButtonBackground),
          backgroundColor: MaterialStateProperty.all<Color>(
            EasyColors.loginButtonBackground,
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () {
          // calls action on click
          action(context);
        },
        label: Text(
          text,
          style: const TextStyle(
            color: EasyColors.loginButtonText,
          ),
        ),
      ),
    );
  }
}
