import 'package:app/generated/gen_colors.g.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class InputField extends StatelessWidget {
  // set required params
  InputField(
      {Key? key,
      required this.controller,
      required this.labelText,
      required this.icon,
      required this.obscureText})
      : super(key: key);

  //TextEditingController for accessing input data and validation
  final TextEditingController controller;

  //LabelText which is shown as Placeholder and Headline
  final String labelText;

  //Icon on right side of input
  final IconData icon;

  //obscureText for passwords
  final bool obscureText;

  //validators
  final RegExp emailValidator = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  final RegExp pinValidator = RegExp("^[0-9]");

  @override
  Widget build(BuildContext context) {
    // Colors for border
    OutlineInputBorder focusedBorder = OutlineInputBorder(
      borderSide: const BorderSide(
        color: EasyColors.loginDataInputBorder,
        width: 1.0,
      ),
      borderRadius: BorderRadius.circular(5.0),
    );
    OutlineInputBorder focusedErrorBorder = OutlineInputBorder(
      borderSide: const BorderSide(
        color: EasyColors.loginDataInputBorder,
        width: 1.0,
      ),
      borderRadius: BorderRadius.circular(5.0),
    );
    OutlineInputBorder enabledBorder = OutlineInputBorder(
      borderSide: const BorderSide(
        color: EasyColors.loginDataInputBorder,
        width: 1.0,
      ),
      borderRadius: BorderRadius.circular(5.0),
    );
    OutlineInputBorder errorBorder = OutlineInputBorder(
      borderSide: const BorderSide(
        color: EasyColors.loginDataInputError,
        width: 1.0,
      ),
      borderRadius: BorderRadius.circular(5.0),
    );

    //building component structure
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: 350,
          color: EasyColors.loginDataInputBackground,
          child: TextFormField(
            controller: controller,
            obscureText: obscureText,
            style: const TextStyle(
              color: EasyColors.loginDataInputText,
            ),
            decoration: InputDecoration(
              enabledBorder: enabledBorder,
              errorBorder: errorBorder,
              focusedErrorBorder: focusedErrorBorder,
              focusedBorder: focusedBorder,
              suffixIcon: Icon(
                icon,
                color: EasyColors.loginDataInputIcon,
              ),
              errorStyle: const TextStyle(color: EasyColors.loginDataInputText),
              labelStyle: const TextStyle(
                color: EasyColors.loginDataInputText,
              ),
              labelText: labelText,
            ),
            validator: (text) {
              if (text == null || text.isEmpty) {
                return translate("login.errors.validators.empty");
              }
              if (icon == Icons.email) {
                if (!emailValidator.hasMatch(text)) {
                  return translate("login.errors.validators.not_email");
                }
              }
              if (icon == Icons.pin) {
                if (text.length != 6) {
                  return translate("login.errors.validators.not_token");
                } else {
                  if (!pinValidator.hasMatch(text)) {
                    return translate("login.errors.validators.not_token");
                  }
                }
              }
              return null;
            },
          ),
        ),
      ],
    );
  }
}
