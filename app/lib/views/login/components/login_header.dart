import 'dart:ui';

import 'package:flutter/material.dart';

class LoginHeader extends StatelessWidget {
  const LoginHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //building component structure
    return Container(
      width: window.physicalSize.width,
      margin: const EdgeInsets.only(top: 20),
      height: 60.0,
      child: const Image(
        image: AssetImage('assets/images/itech/logo.png'),
      ),
    );
  }
}
