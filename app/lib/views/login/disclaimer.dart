import 'package:app/generated/gen_colors.g.dart';
import 'package:app/utils/functions.dart';
import 'components/buttons.dart';
import 'package:flutter/material.dart';

import 'components/login_template.dart';
import 'data.dart';
import 'package:flutter_translate/flutter_translate.dart';

class LoginDisclaimerScreen extends StatelessWidget {
  LoginDisclaimerScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return LocalizationProvider(
      state: LocalizationProvider.of(context).state,
      child: LoginTemplate(
        widgets: [
          Container(
            constraints: const BoxConstraints(
              maxWidth: 500,
              maxHeight: 300,
              minHeight: 200,
              minWidth: 300,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: EasyColors.loginDisclaimerBackground,
            ),
            width: MediaQuery.of(context).size.width / 2,
            child: Container(
              margin: const EdgeInsets.all(10),
              child: Column(
                children: [
                  Text(
                    translate("login.pages.info.disclaimer.title"),
                    style: const TextStyle(
                      color: EasyColors.loginDisclaimerText,
                      fontFamily: "Roboto",
                      fontSize: 32,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    translate("login.pages.info.disclaimer.description"),
                    style: const TextStyle(
                      color: EasyColors.loginDisclaimerText,
                      fontFamily: "Roboto",
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
          ),
          const Spacer(),
          InputButton(
            action: (context) => {Navigator.of(context).push(Functions().createRoute(const LoginDataScreen()))},
            text: translate("login.buttons.disclaimer"),
              icon: Icons.check,
          ),
        ],
      ),
    );
  }
}
