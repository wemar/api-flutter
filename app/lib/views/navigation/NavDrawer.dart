import 'package:app/generated/gen_colors.g.dart';
import 'package:app/utils/functions.dart';
import 'package:app/views/holiday/holiday.dart';
import 'package:app/views/home/home.dart';
import 'package:app/views/substitutions/substitutions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

import '../login/welcome.dart';
import '../settings/settings.dart';
import 'components/menu_header.dart';
import 'components/nav_menu_item.dart';

class NavDrawer extends StatelessWidget {
  NavDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // creates navigation Drawer on left side
    return ConstrainedBox(
      constraints: const BoxConstraints(
        minHeight: 200.0,
      ),
      child: Drawer(
        width: 300,
        backgroundColor: EasyColors.navBarBackground,
        child: ListView(
          children: [
            const MenuHeader(),
            NavMenuItem(
                action: createRoute(const HomeScreen(), context),
                title: translate("home.title"),
                icon: Icons.home),
            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 20),
              child: Column(
                children: [
                  const Divider(
                    color: EasyColors.navBarDivider,
                  ),
                  Text(
                    translate("general.nav.sections.plans"),
                    textAlign: TextAlign.left,
                    style: const TextStyle(color: EasyColors.navBarItemText, fontSize: 18),
                  ),
                  NavMenuItem(
                      action: createRoute(const SubstitutionScreen(), context),
                      title: translate("substitutions_table.title"),
                      icon: Icons.calendar_month),
                  NavMenuItem(
                      action: createRoute(const HolidayScreen(), context),
                      title: translate("holiday_table.title"),
                      icon: Icons.calendar_month),
                  const Divider(
                    color: EasyColors.navBarDivider,
                  ),
                ],
              ),
            ),
            NavMenuItem(
                action: createRoute(const SettingsScreen(), context),
                title: translate("settings.title"),
                icon: Icons.settings),
            const Divider(
              color: EasyColors.navBarDivider,
            ),
            NavMenuItem(
                action: createRoute(const LoginWelcomeScreen(), context),
                title: translate("logout.title"),
                icon: Icons.logout),
          ],
        ),
      ),
    );
  }
}

Function createRoute(action, context) {
  return () => {
        Navigator.of(context).pop(),
        Navigator.of(context).pushReplacement(Functions().createRoute(action)),
      };
}
