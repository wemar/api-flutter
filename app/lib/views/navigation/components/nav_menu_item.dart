import 'package:app/generated/gen_colors.g.dart';
import 'package:flutter/material.dart';
class NavMenuItem extends StatelessWidget {
  // NavMenuItem navDrawer (links)

  const NavMenuItem(
      {Key? key, required this.action, required this.title, required this.icon})
      : super(key: key);
  final Function action;
  final String title;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    // represents individual entry in nav drawer
    return ListTile(
      title: Text(
        title,
        style: const TextStyle(
          color: EasyColors.navBarItemText,
        ),
      ),
      leading: IconButton(
        icon: Icon(icon),
        color: EasyColors.navBarItemIcon,
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      onTap: (){
        action();
      },
    );
  }
}
