import 'package:flutter/material.dart';

class MenuHeader extends StatelessWidget {
  const MenuHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // header for navDrawer (logo)
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: const [
          Image(
            image: AssetImage("assets/images/itech/logo.png"),
          ),
        ],
      ),
    );
  }
}
