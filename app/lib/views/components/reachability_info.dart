import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class ReachabilityInfo {
  Widget show() {
    // shows "Service is unreachable Status"
    return Center(
      child: Container(
        margin: const EdgeInsets.all(30),
        width: 250,
        child: Column(
          children: [
            Text(
              translate("errors.reachability.dialog"),
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 20),
            ),
            const SizedBox(height: 20),
            Text(translate("errors.reachability.description"),
                textAlign: TextAlign.center),
          ],
        ),
      ),
    );
  }
}
