import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';

class BaseStructureListview {
  // creates base structure for ListViews with LoadingOverlay and sizes
  Widget show(BuildContext context, List<Widget> children) {
    return LoaderOverlay(
      child: Container(
        padding: const EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 0),
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(bottom: 30),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: children,
            ),
          ),
        ),
      ),
    );
  }
}
