import 'dart:convert' show utf8;

class SubstitutionPlan {
  // model for SubstitutionPlan is used for mapping data into object
  final List<SubstitutionDate> dates;

  const SubstitutionPlan(this.dates);

  factory SubstitutionPlan.fromJson(Map<String, dynamic> json) {
    return SubstitutionPlan(
      (json['dates'] as List).map((date) {
        return SubstitutionDate.fromJson(date);
      }).toList(),
    );
  }

  Map<String, dynamic> toJson() => {
        'dates': dates.map((date) => date.toJson()).toList(),
      };
}

class SubstitutionDate {
  // individual entry
  final String lastUpdate;
  final String weekDay;
  final int date;
  final int nextDay;
  final List<SubstitutionResult> results;

  const SubstitutionDate(
      this.lastUpdate, this.weekDay, this.date, this.nextDay, this.results);

  factory SubstitutionDate.fromJson(Map<String, dynamic> json) {
    return SubstitutionDate(
      json["last_update"],
      json["week_day"],
      json["date"],
      json["next_day"],
      (json['results'] as List).map((i) {
        return SubstitutionResult.fromJson(i);
      }).toList(),
    );
  }

  Map<String, dynamic> toJson() => {
        'last_update': lastUpdate,
        'week_day': weekDay,
        'date': date,
        'next_day': nextDay,
        'results': results.map((result) => result.toJson()).toList(),
      };
}

class SubstitutionResult {
  final String classname;
  final String hour;
  final String room;
  final String time;
  final String info;

  const SubstitutionResult(
      this.classname, this.hour, this.time, this.info, this.room);

  factory SubstitutionResult.fromJson(Map<String, dynamic> json) {
    return SubstitutionResult(
      json["class"],
      json["hour"],
      json["time"],
      utf8.decode(json["info"].runes.toList()),
      json["room"],
    );
  }

  Map<String, dynamic> toJson() => {
        'classname': classname,
        'hour': hour,
        'room': room,
        'time': time,
        'info': info,
      };
}
