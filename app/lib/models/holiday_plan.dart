class HolidayPlan {
  // model for HolidayPlan is used for mapping data into object
  final List<HolidayPlanEntry> entries;

  const HolidayPlan(this.entries);

  factory HolidayPlan.fromJson( List<dynamic> json) {
    return HolidayPlan(
      (json).map((entry) {
        return HolidayPlanEntry.fromJson(entry);
      }).toList(),
    );
  }

  Map<String, dynamic> toJson() => {
        'entries': entries.map((entry) => entry.toJson()).toList(),
      };
}

class HolidayPlanEntry {
  // individual entry
  final DateTime start;
  final DateTime end;
  final int year;
  final String stateCode;
  final String name;
  final String slug;

  HolidayPlanEntry(
      this.start, this.end, this.year, this.stateCode, this.name, this.slug);

  factory HolidayPlanEntry.fromJson(Map<String, dynamic> json) {
    return HolidayPlanEntry(
      DateTime.parse(json["start"]),
      DateTime.parse(json["end"]),
      json["year"],
      json["stateCode"],
      json["name"],
      json["slug"],
    );
  }

  Map<String, dynamic> toJson() => {
        'start': start,
        'end': end,
        'year': year,
        'stateCode': stateCode,
        'name': name,
        'slug': slug
      };
}
