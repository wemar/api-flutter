import 'dart:convert';

import 'package:app/utils/api_functions.dart';
import 'package:app/views/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/functions.dart';

// Is used for all Auth related actions
class AuthController {
  //Check if user is logged in
  void isLoginUser(context, trueCallback, falseCallback) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // get token from SharedPreferences
    String? token = sharedPreferences.getString("authToken");
    if (token == null) {
      logoutUser();
      falseCallback();
    } else {
      checkAuthStatus(context, () {
        trueCallback();
      }, () {
        // user gets logged out if api is not reachable
        falseCallback();
      });
    }
  }

  void saveUserLogin(String token) async {
    //saves te user token in sharedPreferences / localStorage (web)
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("authToken", token);
  }

  Future<void> loginUser(String username, String password, String code,
      context, loaderCallback, failureCallback) async {
    //Login function for User
    //request body
    final body = jsonEncode(
      {
        'username': username,
        'password': password,
        'code': code,
      },
    );
    // call API with params
    ApiFunctions().callAPI('/login', body, "post", context,
        (Response response) async {
      //success
      final data = jsonDecode(response.body);
      // if response returns a token save it
      //hides loading animation
      loaderCallback();
      if (data["token"] != null) {
        saveUserLogin(data["token"]);
        // navigate to HomeScreen
        Navigator.of(context)
            .pushReplacement(Functions().createRoute(const HomeScreen()));
      } else {
        failureCallback();
      }
    }, (statusCode) {
      //hides loading animation
      loaderCallback();
      if (statusCode != "api_offline") {
        //failure
        failureCallback();
      }
    });
  }

  void registerUser(String username, String password, String email, context,
      loaderCallback, successCallback, failureCallback) async {
    //Register function for User
    //request body
    final body = jsonEncode(
      {
        'username': username,
        'password': password,
        'email': email,
      },
    );
    // call API with params
    ApiFunctions().callAPI('/register', body, "post", context, (response) {
      //success
      //hides loading animation
      loaderCallback();
      Functions.showInfoDialog(
        context,
        translate("login.messages.register.dialog"),
        [
          Text(
            translate("login.messages.register.description"),
          ),
        ],
        translate("login.messages.register.action"),
      );
    }, (statusCode) {
      //failure
      //hides loading animation
      loaderCallback();
      if (statusCode != "api_offline") {
        Functions.showInfoDialog(
          context,
          translate("login.errors.register.dialog"),
          [
            Text(
              translate("login.errors.register.description"),
            ),
          ],
          translate("login.errors.register.action"),
        );
      }
    });
  }

  void logoutUser() async {
    // logs user out
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove("authToken");
  }

  void checkAuthStatus(
      BuildContext context, successCallback, failureCallback) async {
    ApiFunctions().callAPI(
      "/authstatus",
      {},
      "get",
      context,
      (response) {
        if (jsonDecode(response.body)["details"] == "Authenticated") {
          successCallback();
        } else {
          failureCallback();
        }
      },
      (statusCode) {
        if (statusCode != "api_offline") {
          {
            Functions.showInfoDialog(
              context,
              translate("errors.internalError.dialog"),
              [
                Text(
                  translate("errors.internalError.description"),
                ),
              ],
              translate("errors.internalError.action"),
            );
          }
        }
      },
    );
  }
}
