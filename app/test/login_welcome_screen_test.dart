import 'package:app/views/login/components/buttons.dart';
import 'package:app/views/login/welcome.dart';
import 'package:flutter_test/flutter_test.dart';

import 'base.dart';

void main() {
  testWidgets("LoginPage button changes frame", (WidgetTester tester) async {
    await tester.pumpWidget(await TestBase().makeTestableWidget(child: const LoginWelcomeScreen()));
    await tester.pumpAndSettle();

    var startButton = find.text("Start");
    expect(startButton.evaluate().isEmpty, false);
    expect(startButton, findsOneWidget);
  });
}
