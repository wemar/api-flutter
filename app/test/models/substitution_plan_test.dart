import 'package:app/models/substitution_plan.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../base.dart';

void main() {
  testWidgets("check if SubstitutionPlan is mapped correct",
      (WidgetTester tester) async {
    await tester
        .pumpWidget(await TestBase().makeTestableWidget(child: Container()));
    await tester.pumpAndSettle();

    SubstitutionPlan substitutionPlan = SubstitutionPlan.fromJson({
      "dates": [
        {
          "date": 20220906,
          "last_update": "06.09.2022 14:52:45",
          "next_day": 20220907,
          "results": [
            {
              "class": "HT2b",
              "hour": "9",
              "info": "mit Moodle-Arbeitsauftrag v. In",
              "room": "59",
              "time": "15:30-16:15"
            }
          ],
          "week_day": "Dienstag"
        },
        {
          "date": 20220907,
          "last_update": "06.09.2022 14:52:45",
          "next_day": 20220908,
          "results": [
            {
              "class": "AVLF",
              "hour": "3 - 4",
              "info": "Br allein",
              "room": "86",
              "time": "09:30-11:00"
            },
            {
              "class": "AVLF",
              "hour": "5 - 6",
              "info": "Br allein",
              "room": "86",
              "time": "11:30-13:00"
            },
            {
              "class": "IT_c",
              "hour": "1",
              "info": "EuR zus. mit Hal, per WebEx",
              "room": "VR1",
              "time": "07:45-08:30"
            },
            {
              "class": "IT_c",
              "hour": "2",
              "info": "EuR zus. mit Hal, per WebEx",
              "room": "VR1",
              "time": "08:30-09:15"
            },
            {
              "class": "IT_c",
              "hour": "3 - 4",
              "info": "EuR zus. mit Hal, per WebEx",
              "room": "VR2 (53)",
              "time": "09:30-11:00"
            },
            {
              "class": "IT_c",
              "hour": "5 - 6",
              "info": "EuR zus. mit Hal, per WebEx",
              "room": "VR2 (53)",
              "time": "11:30-13:00"
            },
            {
              "class": "IT_L",
              "hour": "1 - 4",
              "info": "Vertretung",
              "room": "225",
              "time": "07:45-11:00"
            },
            {
              "class": "IT_m",
              "hour": "3 - 4",
              "info": "Vertretung",
              "room": "148",
              "time": "09:30-11:00"
            },
            {
              "class": "IT_m",
              "hour": "5 - 6",
              "info": "in Mitbetreuung d.. Sr",
              "room": "148",
              "time": "11:30-13:00"
            },
            {
              "class": "IT_o",
              "hour": "1 - 2",
              "info": "Mr allein",
              "room": "202",
              "time": "07:45-09:15"
            },
            {
              "class": "IT_o",
              "hour": "3 - 4",
              "info": "Vertretung",
              "room": "202",
              "time": "09:30-11:00"
            },
            {
              "class": "IT_y",
              "hour": "3 - 4",
              "info": "Fw. / Vertretung",
              "room": "142",
              "time": "09:30-11:00"
            },
            {
              "class": "IT0t",
              "hour": "5 - 6",
              "info": "Vertretung",
              "room": "221",
              "time": "11:30-13:00"
            }
          ],
          "week_day": "Mittwoch"
        }
      ]
    });

    expect(substitutionPlan.dates.length, 2);
    expect(substitutionPlan.dates[0].date, 20220906);
    expect(substitutionPlan.dates[0].nextDay, 20220907);
    expect(substitutionPlan.dates[0].weekDay, "Dienstag");
    expect(substitutionPlan.dates[0].results.length, 1);
    expect(substitutionPlan.dates[0].lastUpdate, "06.09.2022 14:52:45");
  });
}
