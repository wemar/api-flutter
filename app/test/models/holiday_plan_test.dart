import 'package:app/models/holiday_plan.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../base.dart';

void main() {
  testWidgets(
      "check if HolidayPlan is mapped correct", (WidgetTester tester) async {
    await tester
        .pumpWidget(await TestBase().makeTestableWidget(child: Container()));
    await tester.pumpAndSettle();

    HolidayPlan holidayPlan = HolidayPlan.fromJson([
      {
        "start": "2022-01-28T00:00Z",
        "end": "2022-01-29T00:00Z",
        "year": 2022,
        "stateCode": "HH",
        "name": "winterferien",
        "slug": "winterferien-2022-HH"
      },
      {
        "start": "2022-03-07T00:00Z",
        "end": "2022-03-19T00:00Z",
        "year": 2022,
        "stateCode": "HH",
        "name": "osterferien",
        "slug": "osterferien-2022-HH"
      },
      {
        "start": "2022-05-23T00:00Z",
        "end": "2022-05-28T00:00Z",
        "year": 2022,
        "stateCode": "HH",
        "name": "pfingstferien",
        "slug": "pfingstferien-2022-HH"
      },
      {
        "start": "2022-07-07T00:00Z",
        "end": "2022-08-18T00:00Z",
        "year": 2022,
        "stateCode": "HH",
        "name": "sommerferien",
        "slug": "sommerferien-2022-HH"
      },
      {
        "start": "2022-10-10T00:00Z",
        "end": "2022-10-22T00:00Z",
        "year": 2022,
        "stateCode": "HH",
        "name": "herbstferien",
        "slug": "herbstferien-2022-HH"
      },
      {
        "start": "2022-12-23T00:00Z",
        "end": "2023-01-07T00:00Z",
        "year": 2022,
        "stateCode": "HH",
        "name": "weihnachtsferien",
        "slug": "weihnachtsferien-2022-HH"
      }
    ]);

    expect(holidayPlan.entries.length, 6);
    expect(holidayPlan.entries[0].start, DateTime.parse("2022-01-28T00:00Z"));
    expect(holidayPlan.entries[0].end, DateTime.parse("2022-01-29T00:00Z"));
    expect(holidayPlan.entries[0].year, 2022);
    expect(holidayPlan.entries[0].stateCode, "HH");
    expect(holidayPlan.entries[0].name, "winterferien");
    expect(holidayPlan.entries[0].slug, "winterferien-2022-HH");
  });
}
