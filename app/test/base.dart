import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/flutter_translate.dart';

class TestBase{
  Future<Widget> makeTestableWidget({ required Widget child }) async {
    var delegate = await LocalizationDelegate.create(
      fallbackLocale: "de",
      supportedLocales: ["en", "de"],
    );
    return MediaQuery(
      data: const MediaQueryData(),
      child: MaterialApp(
        localizationsDelegates: [
          delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        home: child,
      ),
    );
  }
}