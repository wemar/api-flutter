import 'package:app/utils/string_extension.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("capitalize capitalizes", () async {
    String hallo = "hallo".capitalize();
    expect("Hallo", hallo);
  });
}
