import 'dart:io';

import 'package:app/models/substitution_plan.dart';
import 'package:app/utils/substitution_plan_functions.dart';
import 'package:app/views/login/welcome.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:http/testing.dart';
import 'package:mockito/annotations.dart';

import '../../base.dart';

@GenerateMocks([http.Client])
void main() {
  setUpAll(() {
    HttpOverrides.global = null;
  });

  testWidgets(
    "getSubstitutionsPlan",
    (WidgetTester tester) async {
      await tester.pumpWidget(
        await TestBase().makeTestableWidget(
          child: const LoginWelcomeScreen(),
        ),
      );
      await tester.pumpAndSettle();
      final client = MockClient(
        (_) async => http.Response(
          '{"dates":[]}',
          200,
        ),
      );
      SubstitutionPlanFunctions substitutionPlanFunctions =
          SubstitutionPlanFunctions();
      substitutionPlanFunctions.getSubstitutionsPlan(
        tester.element(
          find.byType(Container).first,
        ),
        (SubstitutionPlan substitutionPlan) {
          expect(true, true);
        },
        () {
          expect(true, false);
        },
      );
    },
  );
}
