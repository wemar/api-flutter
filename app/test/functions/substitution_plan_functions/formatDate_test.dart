import 'package:app/utils/substitution_plan_functions.dart';
import 'package:app/views/login/welcome.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../base.dart';

void main() {
  testWidgets(
    "formatDate",
    (WidgetTester tester) async {
      await tester.pumpWidget(
        await TestBase().makeTestableWidget(
          child: const LoginWelcomeScreen(),
        ),
      );
      await tester.pumpAndSettle();
      SubstitutionPlanFunctions substitutionPlanFunctions =
          SubstitutionPlanFunctions();
      String date = substitutionPlanFunctions.formatDate(20030114, "Dienstag");
      expect(date, "Tuesday, the  14.01.2003");
    },
  );
}
