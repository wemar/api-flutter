import 'package:app/utils/table_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("creates widget", () async {
    Column column = TableFunctions.headerEntry("Hallo", 2);
    expect(column.children.isEmpty, false);
  });
}
