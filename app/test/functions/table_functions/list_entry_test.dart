import 'package:app/utils/table_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("creates widget", () async {
    TableRow tableRow = TableFunctions.listEntry([], 2);
    expect(tableRow.children, []);
  });
}
