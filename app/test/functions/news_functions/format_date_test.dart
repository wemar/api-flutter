import 'package:app/utils/news_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../base.dart';

void main() {
  testWidgets("formats date", (WidgetTester tester) async {
    await tester.pumpWidget(
        await TestBase().makeTestableWidget(child: Container()));
    await tester.pumpAndSettle();

    String date = NewsFunctions().formatDate("14.01.2003");
    expect(date, "14.01.2003");
  });
}
