import 'package:app/utils/holiday_plan_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../base.dart';

void main() {
  testWidgets("formats date", (WidgetTester tester) async {
    await tester.pumpWidget(
        await TestBase().makeTestableWidget(child: Container()));
    await tester.pumpAndSettle();

    String date = HolidayPlanFunctions().formatDate(DateTime.parse("2003-01-14T13:44:57.999542"));
    expect(date, "14.1.2003");
  });
}
